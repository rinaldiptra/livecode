class StudentsController < ApplicationController
    def new
        @student = Student.new  
      end
  
      def create
          student = Student.new(resource_params)
          student.save
          flash[:notice] = 'Student has been created'
          #puts book.errors.messages
          redirect_to students_path
      end
  
      def edit
          @student = Student.find(params[:id])
      end
  
      def update
          @student = Student.find(params[:id])
          @student.update(resource_params)
          flash[:notice] = 'Student has been update'
          redirect_to edit_student_path(@student)
      end
      def destroy
        @student = Student.find(params[:id])
        @student.destroy
        flash[:notice] = 'Book has been deleted'
        redirect_to students_path
    end

    def index
        @student = Student.all
    end

    def show #menampilkan secaraa detail
        id = params[:id]
        @student = Student.find(id)
        # render plain: id
        # render plain: @book.title
    end

    private

    def resource_params
        params.require(:student).permit(:name, :username, :age, :kelas, :addres, :city, :nik)
    end
end
