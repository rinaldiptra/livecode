class ReportsController < ApplicationController
        def new
            @report = Report.new  
          end
      
          def create
              report= Report.new(resource_params)
              report.save
              flash[:notice] = 'Report has been created'
              #puts book.errors.messages
              redirect_to reports_path
          end
      
          def edit
              @report = Report.find(params[:id])
          end
      
          def update
              @report = Report.find(params[:id])
              @report.update(resource_params)
              flash[:notice] = 'Report has been update'
              redirect_to reports_path(@report)
          end
          def destroy
           @report = Report.find(params[:id])
           @report.destroy
            flash[:notice] = 'Report has been deleted'
            redirect_to reports_path
        end
    
        def index
           @report = Report.all
        end
    
        def show #menampilkan secaraa detail
            id = params[:id]
           @report = Report.find(id)
            # render plain: id
            # render plain: @book.title
        end
    
        private
    
        def resource_params
            params.require(:report).permit(:title, :hasil, :mapel, :teacher_id, :student_id)
        end
end
