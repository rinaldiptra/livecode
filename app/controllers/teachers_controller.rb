class TeachersController < ApplicationController
        def new
            @teacher = Teacher.new  
          end
      
          def create
              teacher = Teacher.new(resource_params)
              teacher.save
              flash[:notice] = 'Student has been created'
              #puts book.errors.messages
              redirect_to teachers_path
          end
      
          def edit
              @teacher = Teacher.find(params[:id])
          end
      
          def update
              @teacher = Teacher.find(params[:id])
              @teacher.update(resource_params)
              flash[:notice] = 'Teacher has been update'
              redirect_to teachers_path(@teacher)
          end
          def destroy
            @teacher = Teacher.find(params[:id])
            @teacher.destroy
            flash[:notice] = 'teacher has been deleted'
            redirect_to teachers_path
        end
    
        def index
            @teacher = Teacher.all
        end
    
        def show #menampilkan secaraa detail
            id = params[:id]
            @teacher = Teacher.find(id)
            # render plain: id
            # render plain: @book.title
        end
    
        private
    
        def resource_params
            params.require(:teacher).permit(:nik, :name, :age, :kelas, :mapel)
        end
    end
