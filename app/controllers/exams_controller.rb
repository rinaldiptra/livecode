class ExamsController < ApplicationController
    
        def new
            @exam = Exam.new  
          end
      
          def create
              exam= Exam.new(resource_params)
              exam.save
              flash[:notice] = 'exam has been created'
              #puts book.errors.messages
              redirect_to exams_path
          end
      
          def edit
              @exam = Exam.find(params[:id])
          end
      
          def update
              @exam = Exam.find(params[:id])
              @exam.update(resource_params)
              flash[:notice] = 'exam has been update'
              redirect_to edit_exam_path(@exam)
          end
          def destroy
           @exam = Exam.find(params[:id])
           @exam.destroy
            flash[:notice] = 'Exam has been deleted'
            redirect_to exams_path
        end
    
        def index
           @exam = Exam.all
        end
    
        def show #menampilkan secaraa detail
            id = params[:id]
           @exam = Exam.find(id)
            # render plain: id
            # render plain: @book.title
        end
    
        private
    
        def resource_params
            params.require(:exam).permit(:title, :mapel, :duration, :nilai, :akatif, :level, :student_id)
        end
    

end
