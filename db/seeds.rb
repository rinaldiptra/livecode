# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Student.create(name:'rinaldi putra', username:'aldi', age:17, kelas:'RPL X1', addres:'Kabupaten Bogor', city:'Caringin', nik:11800423 )
Student.create(name:'Ahmad merbabu', username:'ahmad', age:17, kelas:'RPL X1', addres:'Kabupaten Garut', city:'Rancaekek', nik:11800897 )

Exam.create(title:'UH-1', mapel:'B inggris', duration:60, nilai:80, aktif:'Yes', level:'Hard', student_id:11800897 )

Teacher.create(nik:2343, name:'Ahmad merpuah', age:2, kelas:'X11', mapel:'Kejiwaan')

Report.create(title:'rapot pertama', hasil:90, mapel:'sunda', teacher_id:179, student_id:11990)




